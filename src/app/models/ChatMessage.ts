export class ChatMessage {
    author;
    date;
    text;
    // constructor () {
    //     this.author = '';
    //     this.date = 0;
    //     this.text = '';
    // };
    constructor (author: string, text: string) {
        this.author = author;
        this.date = Date.now();
        this.text = text;
    }
}
