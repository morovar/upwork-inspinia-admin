export const shopTestData = {
  "clients" : {
    "-KluRcEB0XlzzOPynngu" : {
      "address" : "6 rue aux our",
      "email" : "JonDirr@mail.co",
      "name" : "JonDirr-jkhk",
      "orderHistory" : "",
      "phone" : "0613604199",
      "status" : "Disabled",
      "statusClass" : false
    },
    "-KlxiYAtA3KOWBH6IypK" : {
      "name" : "Diesel",
      "status" : "Enabled",
      "statusClass" : true
    },
    "-Km1zw2WYKP50mKxbAPp" : {
      "email" : "qeryxozil@hotmail.com",
      "name" : "james",
      "phone" : "",
      "status" : "Enabled",
      "statusClass" : true
    },
    "-KmQGpiUC215VjiAWejm" : {
      "date" : 1497253764095,
      "name" : "Vitas",
      "status" : "Enabled",
      "statusClass" : true
    },
    "-KmXtG9okBgB_q_OKn2q" : {
      "date" : 1497381539731,
      "email" : "lkjkjlkj@kljlkj.com",
      "name" : "kjklj-kjlj",
      "phone" : "090988877879",
      "statusClass" : true
    },
    "-Kmaj9KGH80MdyOO_hSM" : {
      "date" : 1497445999296,
      "email" : "mupyhawiqu@gmail.com",
      "name" : "VivienPennington",
      "phone" : "1234",
      "status" : "Disabled",
      "statusClass" : false
    }
  },
  "orders" : {
    "0" : {
      "availableProducts" : [ {
        "bayQt" : 20,
        "category" : "",
        "description" : "",
        "name" : "Swodoo",
        "price" : 100,
        "quantity" : 20,
        "tag" : "iron"
      } ],
      "clientName" : "JonDirr",
      "date" : 1483351240000,
      "notes" : "Minus accusamus dolorem quia id quisquam",
      "orderId" : 0,
      "orderSum" : 1640,
      "paid" : "unpaid",
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : 1,
      "shopCart" : [ {
        "bayQt" : 6,
        "category" : "armor",
        "description" : "opa",
        "name" : "Shield",
        "price" : "150",
        "quantity" : "10",
        "tag" : "iron"
      }, {
        "bayQt" : 6,
        "category" : "",
        "description" : "",
        "name" : "computer ",
        "price" : "120",
        "quantity" : "10",
        "tag" : ""
      }, {
        "bayQt" : 2,
        "category" : "",
        "description" : "",
        "name" : "Spear",
        "price" : "10",
        "quantity" : "3",
        "tag" : "opa"
      } ],
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : "4"
    },
    "1" : {
      "clientName" : "JonDirr",
      "date" : 1486324840000,
      "notes" : "",
      "orderId" : 1,
      "orderSum" : 100,
      "paid" : "paid",
      "priceMethod" : "",
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : 1,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "2" : {
      "clientName" : "JonDirr",
      "date" : 1488776440000,
      "notes" : "",
      "orderId" : 2,
      "orderSum" : 200,
      "paid" : "paid",
      "priceMethod" : "",
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : 1,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "3" : {
      "clientName" : "JonDirr",
      "date" : 1491444040000,
      "notes" : "",
      "orderId" : 3,
      "orderSum" : 2750,
      "paid" : "paid",
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : 11,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "4" : {
      "clientName" : "JonDirr",
      "date" : 1496307640000,
      "notes" : "",
      "orderId" : 4,
      "orderSum" : 50,
      "paid" : "paid",
      "priceMethod" : "",
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : 1,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "5" : {
      "clientName" : "JonDirr",
      "date" : 1496408440000,
      "notes" : "",
      "orderId" : 5,
      "orderSum" : 10,
      "paid" : "paid",
      "priceMethod" : "",
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : 1,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "6" : {
      "clientName" : "JonDirr",
      "date" : 1496509240000,
      "notes" : "",
      "orderId" : 6,
      "orderSum" : 700,
      "paid" : "paid",
      "priceMethod" : "",
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : 1,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "7" : {
      "clientName" : "JonDirr",
      "date" : 1496797240000,
      "notes" : "",
      "orderId" : 7,
      "orderSum" : 200,
      "paid" : "paid",
      "priceMethod" : "",
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : 1,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "8" : {
      "clientName" : "JonDirr",
      "date" : 1496804440000,
      "notes" : "",
      "orderId" : 8,
      "orderSum" : 500,
      "paid" : "paid",
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : "2",
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "9" : {
      "clientName" : "JonDirr",
      "date" : 1496811640000,
      "notes" : "",
      "orderId" : 9,
      "orderSum" : 25,
      "paid" : "paid",
      "priceMethod" : "",
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : 1,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km0pgZVOHfM7eRijbjy" : {
      "clientName" : "Diesel",
      "date" : 1496826956172,
      "notes" : "",
      "orderId" : 14,
      "orderSum" : 0,
      "paid" : "unpaid",
      "quantity" : 0,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km0q2l7WmpVKJGor4v1" : {
      "clientName" : "JonDirr",
      "date" : 1496827051189,
      "notes" : "",
      "orderId" : 15,
      "orderSum" : 500,
      "paid" : "unpaid",
      "products" : [ "Shield" ],
      "quantity" : 0,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km0qP0WBDijo3hvy0ZK" : {
      "clientName" : "JonDirr",
      "date" : 1496827142351,
      "notes" : "",
      "orderId" : 16,
      "orderSum" : 500,
      "paid" : "unpaid",
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : 0,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km0qzAZanPiWGTeuYK-" : {
      "clientName" : "Diesel",
      "date" : 1496827294545,
      "notes" : "",
      "orderId" : 17,
      "orderSum" : 500,
      "paid" : "unpaid",
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : 0,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km0rALYw-D-2QVvY-Y0" : {
      "clientName" : "Diesel",
      "date" : 1496827344400,
      "notes" : "",
      "orderId" : 18,
      "orderSum" : 500,
      "paid" : "unpaid",
      "products" : [ "Shield" ],
      "quantity" : 0,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km0rJJFhTwAVYzKSVrD" : {
      "clientName" : "Diesel",
      "date" : 1496827381118,
      "notes" : "",
      "orderId" : 19,
      "orderSum" : 1200,
      "paid" : "unpaid",
      "products" : [ "Swodoo" ],
      "quantity" : "12",
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km0sa1TXAO4F0h1mpii" : {
      "clientName" : "Diesel",
      "date" : 1496827715852,
      "notes" : "",
      "orderId" : 20,
      "orderSum" : 2000,
      "paid" : "unpaid",
      "products" : [ "Swodoo" ],
      "quantity" : "20",
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km17BWfe7ckknj7Hp5w" : {
      "clientName" : "Diesel",
      "date" : 1496831804571,
      "notes" : "",
      "orderId" : 17,
      "orderSum" : 100,
      "paid" : "unpaid",
      "products" : [ "Swodoo" ],
      "quantity" : "1",
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km1A4SzFhCrXkPXxvnk" : {
      "clientName" : "JonDirr",
      "date" : 1496832563158,
      "notes" : "",
      "orderId" : 18,
      "orderSum" : 250,
      "paid" : "paid",
      "priceMethod" : [ "Credit card", "Check" ],
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : "1",
      "status" : "done",
      "statusClass" : true,
      "ticketNumber" : ""
    },
    "-Km1F_5oYNe1yXq5tEGX" : {
      "clientName" : "JonDirr",
      "date" : 1496834003474,
      "notes" : "",
      "orderId" : 19,
      "orderSum" : 0,
      "paid" : "unpaid",
      "priceMethod" : "",
      "products" : [ "Swodoo" ],
      "quantity" : 0,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km1FzEyZ_aah_-W7LKV" : {
      "clientName" : "JonDirr",
      "date" : 1496834110553,
      "notes" : "",
      "orderId" : 20,
      "orderSum" : 0,
      "paid" : "unpaid",
      "priceMethod" : "",
      "products" : [ "Swodoo" ],
      "quantity" : 0,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km1G2b3zujbNVMSiEyj" : {
      "clientName" : "JonDirr",
      "date" : 1496834128417,
      "notes" : "",
      "orderId" : 21,
      "orderSum" : 0,
      "paid" : "paid",
      "priceMethod" : "",
      "products" : [ "Shield" ],
      "quantity" : 0,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km1h1FNIsww80RXxIL_" : {
      "clientName" : "Diesel",
      "date" : 1496841462903,
      "notes" : "",
      "orderId" : 22,
      "orderSum" : 3000,
      "paid" : "unpaid",
      "priceMethod" : [ "Credit card", "Check" ],
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : "12",
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km1hYhGEwl9JWB0ql23" : {
      "clientName" : "JonDirr",
      "date" : 1496841599924,
      "notes" : "",
      "orderId" : 23,
      "orderSum" : 182850,
      "paid" : "unpaid",
      "priceMethod" : [ "Cash" ],
      "products" : [ "Shield" ],
      "quantity" : 1219,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km1pD4JumU0ZeZsk0DF" : {
      "clientName" : "Diesel",
      "date" : 1496843608351,
      "notes" : "",
      "orderId" : 24,
      "orderSum" : 0,
      "paid" : "unpaid",
      "priceMethod" : "",
      "products" : [ "Swodoo" ],
      "quantity" : 0,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km1y-9d2rIXxnK3dvFu" : {
      "clientName" : "Diesel",
      "date" : 1496845910744,
      "notes" : "",
      "orderId" : 25,
      "orderSum" : 0,
      "paid" : "paid",
      "priceMethod" : "",
      "products" : [ "Shield" ],
      "quantity" : 0,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-Km2-6p4Ny2K3Mbkmybb" : {
      "clientName" : "james",
      "date" : 1496846466420,
      "notes" : "",
      "orderId" : 26,
      "orderSum" : 250,
      "paid" : "unpaid",
      "products" : [ "Swodoo", "Shield" ],
      "quantity" : 1,
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-KmNNww9_L6DO3sfGD-u" : {
      "availableProducts" : [ {
        "bayQt" : 10,
        "category" : "",
        "description" : "",
        "name" : "Swodoo",
        "price" : 100,
        "quantity" : 20,
        "tag" : "iron"
      } ],
      "clientName" : "Diesel",
      "date" : 1497205296980,
      "notes" : "",
      "orderId" : 27,
      "orderSum" : 1530,
      "paid" : "unpaid",
      "priceMethod" : [ "Cash" ],
      "quantity" : 0,
      "shopCart" : [ {
        "bayQt" : "3",
        "category" : "",
        "description" : "",
        "name" : "Spear",
        "price" : "10",
        "quantity" : "3",
        "tag" : "opa"
      }, {
        "bayQt" : "10",
        "category" : "armor",
        "description" : "opa",
        "name" : "Shield",
        "price" : "150",
        "quantity" : "10",
        "tag" : "iron"
      } ],
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-KmPsnEGV36r9B4FbkAN" : {
      "availableProducts" : [ {
        "bayQt" : 20,
        "category" : "",
        "description" : "",
        "name" : "Swodoo",
        "price" : 100,
        "quantity" : 20,
        "tag" : "iron"
      } ],
      "clientName" : "Diesel",
      "date" : 1497247200280,
      "notes" : "",
      "orderId" : 28,
      "orderSum" : 1530,
      "paid" : "unpaid",
      "priceMethod" : [ "Check", "bitcoins", "dept" ],
      "quantity" : 0,
      "shopCart" : [ {
        "bayQt" : "10",
        "category" : "armor",
        "description" : "opa",
        "name" : "Shield",
        "price" : "150",
        "quantity" : "10",
        "tag" : "iron"
      }, {
        "bayQt" : "3",
        "category" : "",
        "description" : "",
        "name" : "Spear",
        "price" : "10",
        "quantity" : "3",
        "tag" : "opa"
      } ],
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-KmQRgJIRrhFz_0qz-zD" : {
      "availableProducts" : [ {
        "bayQt" : "10",
        "category" : "armor",
        "description" : "opa",
        "name" : "Shield",
        "price" : "150",
        "quantity" : "10",
        "tag" : "iron"
      } ],
      "clientName" : "Diesel",
      "date" : 1497256609138,
      "notes" : "",
      "orderId" : 29,
      "orderSum" : 0,
      "paid" : "paid",
      "priceMethod" : "",
      "quantity" : 0,
      "shopCart" : [ {
        "bayQt" : 16,
        "category" : "",
        "description" : "",
        "name" : "Swodoo",
        "price" : 100,
        "quantity" : 20,
        "tag" : "iron"
      }, {
        "bayQt" : 1,
        "category" : "",
        "description" : "",
        "name" : "Spear",
        "price" : "10",
        "quantity" : "3",
        "tag" : "opa"
      } ],
      "status" : "done",
      "statusClass" : true,
      "ticketNumber" : ""
    },
    "-KmXsKpItaKGLoWN6r1q" : {
      "availableProducts" : [ {
        "bayQt" : 20,
        "category" : "",
        "description" : "",
        "name" : "Swodoo",
        "price" : 100,
        "quantity" : 20,
        "tag" : "iron"
      }, {
        "bayQt" : "3",
        "category" : "",
        "description" : "",
        "name" : "Spear",
        "price" : "10",
        "quantity" : "3",
        "tag" : "opa"
      }, {
        "bayQt" : "10",
        "category" : "",
        "description" : "",
        "name" : "computer ",
        "price" : "120",
        "quantity" : "10",
        "tag" : ""
      } ],
      "clientName" : "james",
      "date" : 1497381296690,
      "notes" : "",
      "orderId" : 30,
      "orderSum" : 0,
      "paid" : "unpaid",
      "priceMethod" : "",
      "quantity" : 0,
      "shopCart" : [ {
        "bayQt" : "10",
        "category" : "armor",
        "description" : "opa",
        "name" : "Shield",
        "price" : "150",
        "quantity" : "10",
        "tag" : "iron"
      } ],
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    },
    "-KmajXUlGYTudVzgqzGE" : {
      "availableProducts" : [ {
        "bayQt" : 20,
        "category" : "",
        "description" : "",
        "name" : "Swodoo-345",
        "price" : 100,
        "quantity" : 20,
        "tag" : "iron"
      }, {
        "bayQt" : "10",
        "category" : "armor",
        "description" : "opa",
        "name" : "Shield",
        "price" : "150",
        "quantity" : "10",
        "tag" : "iron"
      }, {
        "bayQt" : "3",
        "category" : "",
        "description" : "",
        "name" : "Spear",
        "price" : "10",
        "quantity" : "3",
        "tag" : "opa"
      }, {
        "bayQt" : "10",
        "category" : "",
        "description" : "",
        "name" : "computer ",
        "price" : "120",
        "quantity" : "10",
        "tag" : ""
      }, {
        "bayQt" : "5",
        "category" : "shampoo",
        "description" : "",
        "name" : "shampoo delux",
        "price" : "10",
        "quantity" : "5",
        "tag" : "shampoo cream"
      } ],
      "clientName" : "Diesel",
      "date" : 1497446098283,
      "notes" : "",
      "orderId" : 31,
      "orderSum" : 0,
      "paid" : "paid",
      "priceMethod" : "",
      "quantity" : 0,
      "shopCart" : [ {
        "bayQt" : "1",
        "category" : "",
        "description" : "",
        "name" : "coupe ",
        "price" : "30",
        "quantity" : "1",
        "tag" : ""
      } ],
      "status" : "pending",
      "statusClass" : false,
      "ticketNumber" : ""
    }
  },
  "products" : {
    "-KluRjKOciCgTxZNvYvz" : {
      "category" : "",
      "description" : "",
      "name" : "Swodoo-345",
      "price" : 100,
      "quantity" : 20,
      "tag" : "iron"
    },
    "-KluXtvG_METJ0Kj6B_k" : {
      "category" : "armor",
      "description" : "opa",
      "name" : "Shield",
      "price" : "150",
      "quantity" : "10",
      "tag" : "iron"
    },
    "-Km1TIsjJID9WfsK3Nwg" : {
      "category" : "",
      "description" : "",
      "name" : "Spear",
      "price" : "10",
      "quantity" : "3",
      "tag" : "opa"
    },
    "-KmR8MNYHvCSgjc-r-nc" : {
      "category" : "",
      "description" : "",
      "name" : "computer ",
      "price" : "120",
      "quantity" : "10",
      "tag" : ""
    },
    "-Km_KXdqIu8WVGS7OEmC" : {
      "category" : "shampoo",
      "description" : "",
      "name" : "shampoo delux",
      "price" : "10",
      "quantity" : "5",
      "tag" : "shampoo cream"
    },
    "-KmajRv4w7fN0r0z5NwN" : {
      "category" : "",
      "description" : "",
      "name" : "coupe ",
      "price" : "30",
      "quantity" : "1",
      "tag" : ""
    },
    "-Kmw4s8Ew5celCEHjqo7" : {
      "category" : "armor",
      "description" : "",
      "isService" : true,
      "name" : "opa1",
      "price" : "10",
      "quantity" : 0,
      "tags" : "opa,popa"
    },
    "-Kmw8Ug8lEab0GZp6nZv" : {
      "category" : "wepon",
      "description" : "",
      "isService" : true,
      "name" : "asasssa23",
      "price" : "456",
      "quantity" : 0,
      "tags" : "opa,popa"
    },
    "-KmwBtfrQwW4lTCQoqtQ" : {
      "category" : "56",
      "description" : "",
      "isService" : true,
      "name" : "tags",
      "price" : "1",
      "quantity" : 0,
      "tags" : [ "asdsd", "asd", "12" ]
    },
    "-KmwCmvHMAGj1il7ySAY" : {
      "category" : "1",
      "description" : "",
      "isService" : true,
      "name" : "tags2",
      "price" : "3",
      "quantity" : 0
    },
    "-KmwDRlryfmUMnoqHQQI" : {
      "category" : "werew",
      "description" : "",
      "isService" : true,
      "name" : "2323",
      "price" : "23",
      "quantity" : 0,
      "tags" : [ "1", "3", "qwewe" ]
    },
    "-Kn-0mfc1YieaMVSwGzE" : {
      "category" : "123",
      "description" : "",
      "isService" : false,
      "name" : "opa12",
      "price" : "10",
      "quantity" : "3",
      "stockAlert" : 5,
      "tags" : [ "1", "2", "3", "4" ]
    },
    "-Kn-34GBJrQNVHhJmFl_" : {
      "category" : "werew",
      "description" : "",
      "isService" : false,
      "name" : "lopata",
      "price" : "45",
      "quantity" : "34",
      "stockAlert" : 12,
      "tags" : [ "r", "1", "asdasd", "adas", "asdsad", "opa" ]
    },
    "-Kn-WUbBExf51pqfkYds" : {
      "category" : "werew",
      "description" : "",
      "isService" : true,
      "name" : "lopata",
      "price" : "45",
      "quantity" : "5",
      "stockAlert" : 12,
      "tags" : [ "r", "1" ]
    },
    "-Kn10rFKJsh6ZffWMnYo" : {
      "category" : "o",
      "description" : "",
      "isService" : true,
      "name" : "ououo",
      "price" : "123",
      "tags" : [ "ZX", "ZXZX" ]
    },
    "-Kn3UqlEydiOecgS__qN" : {
      "category" : "product ",
      "description" : "",
      "name" : "product 1",
      "price" : "10",
      "quantity" : "10",
      "stockAlert" : 8,
      "tags" : [ "product" ]
    }
  }
}