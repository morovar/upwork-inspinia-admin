import { Component, OnInit } from '@angular/core';
import { environment } from "../environments/environment";

declare var Stripe: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  ngOnInit() {
    Stripe.setPublishableKey(environment.stripe_pub_key);
  }
}
