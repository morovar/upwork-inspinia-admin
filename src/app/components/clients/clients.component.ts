import {Component, OnInit} from '@angular/core';
import {NgForm, FormControl, FormGroup, Validators} from '@angular/forms';
import {footable} from '../../app.helpers';
import {summernote} from '../../app.helpers';
import {slimscroll} from '../../app.helpers';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import {Subject} from 'rxjs/Subject';
import {Router} from '@angular/router';
import {AuthGuard} from '../auth.service';

declare var jQuery: any;
declare var $: any;
declare var FooTable: any;

export class Client {
  $key;
  name;
  phone;
  email;
  status;
  address;
  statusClass;
  date;
  birhday;
  city;
  zipcode;

  console() {
    this.zipcode = '';
    this.name = '';
    this.phone = '';
    this.email = '';
    this.status = 'Enabled';
    this.address = '';
    this.birhday = '';
    this.city = '';
    this.statusClass = true;
    this.date = Date.now();
  }
}

@Component({
  selector: 'clients',
  templateUrl: 'clients.template.html'
})

export class ClientsComponent {
  name2;
  onEditSubmit;

  crClientModel = new Client();
  edClientModel = new Client();

  orderHistory;

  statusClass;
  clients: FirebaseListObservable<any[]>;
  clientsOrders: FirebaseListObservable<any[]>;
  clientsOrdersValue: any[];
  filterOrdersSubject = new Subject();
  filteredOrders: any;

  client;

  newClient: any = {status: 'Enabled'};
  searchClient;

  selectedClient: any = {};
  selectedRowIdx = -1;

  // disable controll buttons if shop is unpaid
  dsblCtrlBtns = false;

  //clients table source
  clientsTblSrc = [];

  // shop logs
  activityLog;

  footableRef;

  constructor(public db: AngularFireDatabase,
              private router: Router,
              private authServ: AuthGuard) {
    const self = this;
    if (authServ.userInfo.status === 'unpaid') {
      this.dsblCtrlBtns = true;
    }
    this.clients = db.list(`/shops/${authServ.userId}/clients`);
    this.clients.subscribe(
      clients => {
        console.log('subscription next');
        this.clientsTblSrc = clients;
        console.log(clients);
        setTimeout(() => {
          // if (self.footableRef) {
          //   console.log('redraw');
          //   $('.footable').trigger('footable_redraw');
          //   this.footableRef.destroy('.footable');
          // }

          self.footableRef = FooTable.init('.footable');
        }, 400);
      }
    );

    this.clientsOrders = db.list(`/shops/${authServ.userId}/orders`);
    this.activityLog = db.list(`/shops/${authServ.userId}/activityLog`);
    this.clientsOrders.subscribe(queriedItems => {
      this.clientsOrdersValue = queriedItems;
    });
  }

  onOrdersHistory(clientName: string) {
    this.filterOrdersSubject.next(clientName);
  }

  ngOnInit(): any {
    const self = this;

    $('#birthday').datetimepicker({
      format: 'DD/MM/YYYY',
      maxDate : (new Date())
    })
    .on('dp.change', (e) => {
      self.crClientModel.birhday = e.date.unix() * 1000;
    });

    $('#ebirthday').datetimepicker({
      format: 'DD/MM/YYYY',
      maxDate : (new Date())
    })
    .on('dp.change', (e) => {
      self.edClientModel.birhday = e.date.unix() * 1000;
    });

    summernote();
    slimscroll();
  }

  onRowClick(i, client) {
    this.selectedClient = client;
    this.selectedRowIdx = i;
    this.filteredOrders = this.clientsOrdersValue.filter(order => order.clientName == client.name);
  }

  onCreate(form: NgForm) {
    const client = this.crClientModel;
    client.date = Date.now();
    // prepare order object for saving
    client.statusClass = true;
    if (client.status === 'Disabled') {
      client.statusClass = false;
    }

    // send to firebase
    this.clients.push(client);
    this.activityLog.push({date: Date.now(), activity: 'new client', name: client.name});

    // close modal
    $('#create-form').modal('toggle');

    // reset form
    form.resetForm();
    this.crClientModel = new Client();
  }

  onGet(keyClient) {
    this.db.object(`/shops/${this.authServ.userId}/clients/${keyClient}`).subscribe(snapshot => {
      this.edClientModel = snapshot;
      $('#ebirthday').data('DateTimePicker').date(new Date(this.edClientModel.birhday));
      // filter clients orders
      this.filteredOrders = this.clientsOrdersValue.filter(order => order.clientName == snapshot.name);
    });
  }

  onUpdate(form: NgForm) {
    const client = this.edClientModel;

    // prepare order object for saving
    client.statusClass = true;
    if (client.status === 'Disabled') {
      client.statusClass = false;
    }

    // send to firebase
    this.clients.update(client.$key, client);
    this.activityLog.push({date: Date.now(), activity: 'edit client', key: client.$key});

    // close modal
    $('#edit-form').modal('toggle');

    // reset form
    form.resetForm();
    this.edClientModel = new Client();
  }
}
