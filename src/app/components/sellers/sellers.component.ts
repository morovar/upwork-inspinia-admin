import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { NgForm } from '@angular/forms';
import { footable } from '../../app.helpers';
import { Router } from '@angular/router';
import { shopTestData } from '../../data/shoptestdata';
import { AuthGuard } from '../auth.service';

declare var $: any;
declare var FooTable:any;

class User {
  $key;
  name;
  email;
  role;
  routes;

  constructor() {
    this.name = '';
    this.email = '';
    this.role = 'seller';
    this.routes = [];
  }
}

@Component({
  selector: 'app-sellers',
  templateUrl: './sellers.component.html',
  styleUrls: ['./sellers.component.css']
})

export class SellersComponent implements OnInit {
  search;
  // FirebaseListObservable<any>;
  users = [];
  usersRef: FirebaseListObservable<any>;
  roles = [];
  model = new User();
  createFlag = false;
  password = '';
  userForDelete;
  // disable controll buttons if shop is unpaid
  dsblCtrlBtns = false;
  constructor(
    public db: AngularFireDatabase,
    private router: Router,
    private authServ: AuthGuard,
    private afAuth: AngularFireAuth
  ) {
    if ( authServ.userInfo.status === 'unpaid' ) {
      this.dsblCtrlBtns = true;
    }
    this.usersRef = db.list(`/shops/${authServ.userId}/users`);

    this.db.list(`/shops/${authServ.userId}/users`).subscribe(
      users => {
        this.users = users;
        console.log(users);

        setTimeout(() => {FooTable.init('#footable')} , 500 );
      }
    );
  }

  ngOnInit() {
    // footable();
  }

  onEdit(user) {
    console.log(user);
    this.createFlag = false;
    console.log(user);
    this.model = user;
  }

  onSave(form: NgForm) {
    this.createFlag = false;
    // update user
    if (this.model.$key) {
      this.usersRef.update(this.model.$key, this.model);
    // create user
    } else {
      this.usersRef.push(this.model);
      // firebase auth
      this.createFirebaseUser(this.model.email, this.password);
    }
    // close modal
    $('#user-form').modal('toggle');

    // reset form
    form.resetForm();
    this.model = new User();
  }

  onCreate(from: NgForm) {

    this.createFlag = true;
    from.resetForm();
    this.model = new User();
  }

  onDelete(user) {
    console.log(user);

    this.usersRef.remove(user.$key);
  }

  createFirebaseUser(email: string, password: string) {
    this.afAuth.auth.createUserWithEmailAndPassword(email, password)
    .then(
      (user) => {
        console.log('firebase user created');
        this.sendEmailVerification(user);
      }
    )
    .catch(
      err => console.log('creating user error ' + err)
    );
  }

  sendEmailVerification(firebaseUser) {
    firebaseUser.sendEmailVerification()
    .then( () => {
      console.log('email sendet');
    })
    .catch(
      () => console.log('erroremail sendet')
    );
  }

  onRightChange(user, event) {
    console.log('onRightChange');
    if (!user.routes || user.routes == null) {
      user.routes = [];
    }

    if (!event.target.checked) {
      user.routes.push(event.target.value);
    } else {
      const idx = user.routes.indexOf(event.target.value);
      if (idx !== -1) {
        user.routes.splice(idx, 1);
      }
    }

    this.usersRef.update(user.$key, user);
  }

  ifChecked(user, route): boolean {
    // console.log('if checked');
    if (user.routes && user.routes.length > 0) {
      return user.routes.indexOf(route) === -1;
    }
    return true;
  }
}
