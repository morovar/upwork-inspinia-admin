import { Component, OnInit, ViewEncapsulation, NgZone} from '@angular/core';
import { HttpModule, JsonpModule , Http} from '@angular/http';
import { AuthGuard } from '../auth.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

declare var $: any;

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.css'],
  encapsulation: ViewEncapsulation.None,
})

export class SubscriptionComponent implements OnInit {
  plan;
  cardNumber: string;
  expiryMonth: string;
  expiryYear: string;
  cvc: string;
  message;

  constructor(private _zone: NgZone,
  private http: Http,
  private authServ: AuthGuard) {}

  getToken() {
    this.message = 'Loading...';

    (<any>window).Stripe.card.createToken({
      number: this.cardNumber,
      exp_month: this.expiryMonth,
      exp_year: this.expiryYear,
      cvc: this.cvc
    }, (status: number, response: any) => {

      // Wrapping inside the Angular zone
      this._zone.run(() => {
        if (status === 200) {
          this.message = `Success! Card token ${response.card.id}`;
        } else {
          this.message = response.error.message;
        }
      });
    });
  }

  isStripeResponseWait = false;
  isCongrat = false;
  stripeEror = false;
  stripeErrorMessage = '';
  createSubscription(plan) {
    // setTimeout(() => {
    //   $('#congrat').modal('toggle');
    // }, 3000);
    console.log('subscribe', plan);
    // localhost:5000/crcustasubscr
    // https://stripe-srv-side.herokuapp.com/crcustasubscr
    this.isStripeResponseWait = true;
    this.http.post('https://stripe-srv-side.herokuapp.com/crcustasubscr',
      {
        'email': this.authServ.userInfo.email,
        'sbscrid': plan,
        'source': {
          'object': 'card',
          'number': this.cardNumber,
          'exp_month': this.expiryMonth,
          'exp_year': this.expiryYear,
          'cvc': this.cvc
        }
      }
    ).map(
      res => {
        this.isStripeResponseWait = false;
        let body = res.json();
        console.log('subscription');
        console.log(body);
        if (body.sub) {
          this.authServ.updateUserSettings({status: 'paid', stripe_subscription: body.sub});
          // $('#congrat').modal('toggle');
          this.isCongrat = true;
        } else {
          this.stripeEror = true;
          this.stripeErrorMessage = body;
        }
      }
    ).subscribe(
      r => console.log(r),
      e => console.log(e)
    );
  }

  ngOnInit() {
  }

  onTsest()
  {
    console.log('test');
    $('#congrat').modal('toggle');
  }
}
