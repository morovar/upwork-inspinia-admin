import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html'
})

export class ConfirmEmailComponent implements OnInit {

  constructor(private router: Router) {
    setInterval(() => router.navigate(['/login']), 5000);
  }

  ngOnInit() {
  }

}
