import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AuthGuard } from '../auth.service';
import { ChatMessage } from '../../models/ChatMessage';

@Component({
  selector: 'app-adminmess',
  templateUrl: './adminmess.component.html',
  styleUrls: ['./adminmess.component.css']
})
export class AdminmessComponent implements OnInit {
  message;
  selfName = '';
  users = this.db.list('/users');
  currentChat: FirebaseListObservable<any>;
  constructor(
    public db: AngularFireDatabase,
    private authServ: AuthGuard
    ) {

     }

  ngOnInit() {
    this.authServ.fetchUserSettings().subscribe (
      sett =>  this.selfName = sett.name
    );
  }

  onSendChatMessage(text) {
    console.log(`send message ${text}`);
    const mess = new ChatMessage(this.selfName, text);
    this.currentChat.push(mess);
  }

  onChangeChatUser(uid) {
    console.log('change chat user ' + uid);
    this.currentChat = this.db.list(`/chats/${uid}`);
  }
}
