import {Component, OnInit} from '@angular/core';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import {footable} from '../../app.helpers';
import {Router} from '@angular/router';
import {shopTestData} from '../../data/shoptestdata';
import {Observable} from "rxjs/Observable";

declare var $: any;
declare var FooTable: any;

class Shop {
  $key;
  name;
  manager;
  products;
  clients;
  orders;
  created;
  testData;
  constructor() {
    this.testData= true;
    this.created = 0;
    this.name = '';
    this.manager = {};
    this.products = [];
    this.clients = [];
    this.orders = [];
  }
}

@Component({
  selector: 'app-shops',
  templateUrl: './shops.component.html',
  styleUrls: ['./shops.component.css']
})

export class ShopsComponent implements OnInit {
  testData;
  shopForDelete;
  s;
  search;
  users: FirebaseListObservable<any>;
  $ = $;
  shops: Observable<any>;
  searchShop;
  model = new Shop();

  constructor(public db: AngularFireDatabase,
              private router: Router) {
    this.users = db.list('/users');
    this.shops = db.list('/shops').do(
      () => setTimeout(() => {
        FooTable.init('#footable');
      }, 500)
    )
  }

  ngOnInit() {
  }

  onCreate(form) {
    console.log(this.model);
    this.model.products = shopTestData.products;
    this.model.clients = shopTestData.clients;
    this.model.orders = shopTestData.orders;
    this.model.created = Date.now();
    // close modal
    $('#create-form').modal('toggle');
    this.db.list('/shops').push(this.model);
    // reset form
    form.resetForm();
    this.model = new Shop();
  }

  onDeleteBtn(id){
    // this.shopForDelete = id;
    $('#v-shop').modal('hide');
    setTimeout(()=>$('#confirmation').modal('show'),500);
  }


  onDelete(shopForDelete) {
    // console.log(shopForDelete.$key);
    this.db.list('/shops').remove(this.shopModel.$key);
  }

  countKeys(object) {
    if (!object) {
      return 0;
    }
    return Object.keys(object).length;
  }

  shopActivity;

  shopModel;
  onView(shop) {
    this.shopActivity=[];
    console.log('onView');
    this.shopModel = shop;
    console.log(shop);
    // console.log(shop.activityLog.values);
    if (shop.activityLog) {
      this.shopActivity = Object.keys(shop.activityLog).map(key => shop.activityLog[key]);
    }
    // this.db.list(`/shops/${shop.$key}/activityLog`).subscribe(
    //   log => {}
    // );
  }
}
