import { Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-i-check',
  template: `<div class="state icheckbox_square-green" [class.checked]="_value" (click)="toggle()"></div>`,
  styleUrls: ['./i-check.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => iCheckComponent),
      multi: true
    }
  ]
})
export class iCheckComponent implements ControlValueAccessor {
  // tslint:disable-next-line:no-input-rename
  @Input('value') _value = false;

  onChange: any = () => { };
  onTouched: any = () => { };
  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }

  constructor() {
   }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    if (value) {
      this.value = value;
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  toggle(){
    this._value = !this._value;
    this.onChange(this._value);
    this.onTouched();
  }
}
