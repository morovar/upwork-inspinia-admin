import { Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-tags-list',
  templateUrl: './tags-list.component.html',
  styleUrls: ['./tags-list.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TagsListComponent),
      multi: true
    }
  ]
})
export class TagsListComponent implements ControlValueAccessor {
  // tslint:disable-next-line:no-input-rename
  @Input('value') _value = [];

  onChange: any = () => { };
  onTouched: any = () => { };
  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }

  constructor() {
    console.log('app-tags-list');
   }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    console.log(value);
    if (value) {
      this.value = value;
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  addValue(tag) {
    if (this._value) {
      if (this._value.indexOf(tag) === -1) {
        this._value.push(tag);
      }
    }
  }

  removeValue(i) {
    this._value.splice(i, 1);
  }
}
