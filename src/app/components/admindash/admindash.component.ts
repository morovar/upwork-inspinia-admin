import {Component, OnInit} from '@angular/core';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import {Router} from '@angular/router';
import {AuthGuard} from '../auth.service';
import {Observable} from "rxjs/Observable";
import {Http} from "@angular/http";
import "rxjs/add/operator/count";
import "rxjs/add/operator/filter";
import "rxjs/add/operator/take";

declare var FooTable: any;
declare var moment: any;

@Component({
  selector: 'app-admindash',
  templateUrl: './admindash.component.html',
  styleUrls: ['./admindash.component.css']
})

export class AdmindashComponent implements OnInit {
  stockAlertProducts;
  shops: Observable<any>;
  todolist: FirebaseListObservable<any>;

  // shops = [];
  totalShops = 0;
  monthlyIncome : Observable<any>;
  monthlyShops;

  constructor(public db: AngularFireDatabase,
              private router: Router,
              private authServ: AuthGuard,
              private http:Http) {
    this.shops = db.list('/shops')
      .do((snapshot) => setTimeout(() => {
        FooTable.init('#footable');
        this.totalShops = snapshot.length;
      }, 500));
    this.todolist = db.list('/todolist');
    //
    // http://localhost:5000/charges
    this.monthlyIncome = this.http.get('https://stripe-srv-side.herokuapp.com/charges').map(
        (resp) => resp.json().value / 100
    );

    const currMonth = moment().startOf('month').unix() * 1000;
    db.list('/shops')
      .subscribe(
        (snap) => {
          this.monthlyShops = snap.filter((sh) => sh.created?sh.created >= currMonth:false).length;
        }
      )

  }
  // this.monthlyIncome.subscribe((v)=>console.log(v));
  // this.monthlyShops.subscribe((n)=>console.log(n));

  ngOnInit() {
  }

  countKeys(object) {
    if (!object) {
      return 0;
    }
    return Object.keys(object).length;
  }


}
