import {Component, OnInit, ViewEncapsulation,ElementRef,Renderer2} from '@angular/core';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import {NgForm} from '@angular/forms';
import {AuthGuard} from '../auth.service';

declare var $: any;

export class RendezvousEvent {
  key;
  $key;
  client;
  user;
  description;
  start;
  end;
  source;

  constructor() {
    this.client = '';
    this.user = '';
    this.description = '';
    this.start = Date.now();
    this.end = Date.now();
  }
}

@Component({
  selector: 'app-shopcalendar',
  templateUrl: './shopcalendar.component.html',
  styleUrls: ['./shopcalendar.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class ShopcalendarComponent implements OnInit {
  newEventModel = new RendezvousEvent();

  calendarOptions: Object = {
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    height: 'parent',
    fixedWeekCount: false,
    defaultDate: '2016-09-12',
    // editable: true,
    eventLimit: true, // allow "more" link when too many events
    events: [
      {
        title: 'All Day Event',
        start: '2016-09-01'
      },
      {
        title: 'Long Event',
        start: '2016-09-07',
        end: '2016-09-10'
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: '2016-09-09T16:00:00'
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: '2016-09-16T16:00:00'
      },
      {
        title: 'Conference',
        start: '2016-09-11',
        end: '2016-09-13'
      },
      {
        title: 'Meeting',
        start: '2016-09-12T10:30:00',
        end: '2016-09-12T12:30:00'
      },
      {
        title: 'Lunch',
        start: '2016-09-12T12:00:00'
      },
      {
        title: 'Meeting',
        start: '2016-09-12T14:30:00'
      },
      {
        title: 'Happy Hour',
        start: '2016-09-12T17:30:00'
      },
      {
        title: 'Dinner',
        start: '2016-09-12T20:00:00'
      },
      {
        title: 'Birthday Party',
        start: '2016-09-13T07:00:00'
      },
      {
        title: 'Click for Google',
        url: 'http://google.com/',
        start: '2016-09-28'
      }
    ]
  };

  events: FirebaseListObservable<any[]>;
  users: FirebaseListObservable<any[]>;
  clients: FirebaseListObservable<any[]>;
  domCalendarElement;
  clientFilter;

  // disable controll buttons if shop is unpaid
  dsblCtrlBtns = false;

  isNewClient = false;
  newClientName = '';
  newClientMail = '';
  startSelected = false;
  editMode = false;
  selUser = -2;
  constructor(public db: AngularFireDatabase,
              public authServ: AuthGuard,
              private rd: Renderer2) {
    if (authServ.userInfo.status === 'unpaid') {
      this.dsblCtrlBtns = true;
    }
    this.events = db.list(`/shops/${authServ.userId}/events`);
    this.users = db.list(`/shops/${authServ.userId}/users`);
    this.clients = db.list(`/shops/${authServ.userId}/clients`);

  }

  ngOnInit() {
    // $(".user-btn").click(() => {
    //   $(this).addClass("act-user").siblings().removeClass("act-user");
    // });

    this.events.subscribe(
      snapshot => {
        // console.log('clientFilter ' + self.clientFilter);

        let filtered = snapshot.filter(event => event.client === self.clientFilter);
        let fullCalendarFormat = filtered.map(e => {
          e.title = `${e.client}-${e.user}`;
          e.key = e.$key;
          return e;
        });
        $('#calendar').fullCalendar('updateEvents', fullCalendarFormat);
      }
    );

    const self = this;
    const stDTPicker = $('#start-datetimepicker');
    const enDTPicker = $('#end-datetimepicker');
    stDTPicker.datetimepicker();
    enDTPicker.datetimepicker();
    stDTPicker.data('DateTimePicker').minDate(new Date());

    stDTPicker.on('dp.change', (e) => {
      self.newEventModel.start = e.date.unix() * 1000;
      enDTPicker.data('DateTimePicker').minDate(e.date);
      console.log(self.newEventModel);
      enDTPicker.data('DateTimePicker').date(e.date.add(0.5, 'hours'));
      self.startSelected = true;
    });

    enDTPicker.on('dp.change', (e) => {
      self.newEventModel.end = e.date.unix() * 1000;
      console.log(self.newEventModel);
    });

    this.domCalendarElement = $('#calendar');
    this.domCalendarElement.fullCalendar({
      header: {
        left: 'prev,next,today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      // edit event
      eventClick: (rndzv, jsEvent, view) => {
        console.log(rndzv);
        self.newEventModel = rndzv;
        stDTPicker.data('DateTimePicker').date(new Date(rndzv.start));
        enDTPicker.data('DateTimePicker').date(new Date(rndzv.end));
        self.startSelected = true;
        $('#new-event').modal('toggle');
        this.editMode = true;
      },
      defaultDate: Date.now(),
      defaultView: 'basicDay',
      editable: true,
      events: function (start, end, timezone, callback) {
        console.log(`start: ${start} end: ${end} timezone: ${timezone}`, self.clientFilter ? self.clientFilter : 'client filter is null');
        self.events.take(1).subscribe(
          snapshot => {
            if (self.clientFilter) {
              console.log('clientFilter ' + self.clientFilter);
              let filtered = snapshot.filter(event => event.client === self.clientFilter);
              let fullCalendarFormat = filtered.map(e => {
                e.title = `${e.client}-${e.user}`;
                e.key = e.$key;
                return e;
              });
              callback(fullCalendarFormat);
            } else {
              callback(snapshot.map(e => {
                e.title = `${e.client}-${e.user}`;
                e.key = e.$key;
                return e;
              }));
            }
          }
        );
      }
    });
  }

  addEvent(form: NgForm) {
    this.newEventModel.user = this.authServ.userInfo.name;
    delete this.newEventModel.source;
    if (this.isNewClient) {
      this.db.list(`/shops/${this.authServ.userId}/clients`).push(
        {name: this.newClientName, email: this.newClientMail}
      ).then(
        () => {
          console.log(this.newEventModel);
          this.newEventModel.client = this.newClientName;
          if (this.editMode)
            this.events.update(this.newEventModel.key, this.newEventModel);
          else
            this.events.push(this.newEventModel);

          this.newEventModel = new RendezvousEvent();
          form.resetForm();
        }
      );
    } else {
      console.log('addEvent', this.newEventModel);
      if (this.editMode)
        this.events.update(this.newEventModel.key, this.newEventModel);
      else
        this.events.push(this.newEventModel);

      this.newEventModel = new RendezvousEvent();
      form.resetForm();
    }

    $('#calendar').fullCalendar('refetchEvents');
    this.isNewClient = false;
    // reset form
    this.startSelected = false;
    this.editMode = false;
  }

  delEvent(form: NgForm) {
    console.log(this.newEventModel.key);
    this.events.remove(this.newEventModel.key);
    this.editMode = false;
    $('#calendar').fullCalendar('refetchEvents');
  }

  updateCalendar(clientName) {
    console.log('refetchEvents');
    console.log(clientName);

    this.clientFilter = clientName;
    $('#calendar').fullCalendar('refetchEvents');
    // this.domCalendarElement.fullCalendar('rerenderEvents');
    this.editMode = false;
  }
}
