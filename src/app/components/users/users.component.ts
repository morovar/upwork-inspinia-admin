import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { NgForm } from '@angular/forms';
import { footable } from '../../app.helpers';
import { Router } from '@angular/router';
import { shopTestData } from '../../data/shoptestdata';
import { AuthGuard } from '../auth.service';

declare var $: any;

class User {
  $key;
  name;
  email;
  role;
  routes;

  constructor() {
    this.name = '';
    this.email = '';
    this.role = 'admin-user';
    this.routes = [];
  }
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {
  // FirebaseListObservable<any>;
  users = [];
  usersRef: FirebaseListObservable<any>;
  roles = [];
  model = new User();
  createFlag = false;
  password = '';
  search;
  userForDelete;

  constructor(
    public db: AngularFireDatabase,
    private router: Router,
    private authServ: AuthGuard,
    private afAuth: AngularFireAuth
  ) {

    this.usersRef = db.list('/users');

    this.usersRef.subscribe(
      users => this.users = users.filter(u => u.role == 'admin-user')
    );
    // db.list('/roles').subscribe(
    //   roles => {
    //     this.roles = roles;
    //     this.users.forEach(u => {
    //       for (let i = 0; i < this.roles.length; i++) {
    //         const r = this.roles[i];
    //         if (r.$key === u.$key) {
    //           u.role = r.role;
    //         }
    //       }
    //     });
    //   }
    // );
  }

  ngOnInit() {
    // const uid = this.authServ.userId;
    // if ( uid ) {
    //   this.db.list('users').update('C0monvKPZBe3bvYPamG9Y5w1Tq82', {
    //     role: 'admin',
    //     routes: ['clients', 'products',]
    //   });
    // }

    footable();
  }

  onEdit(user) {
    this.createFlag = false;
    console.log(user);
    this.model = user;
  }

  onSave(form: NgForm) {
    this.createFlag = false;
    // update user
    if (this.model.$key) {
      this.db.list('/users').update(this.model.$key, this.model);
    // create user
    } else {
      this.db.list('/users').push(this.model);
      // firebase auth
      this.createFirebaseUser(this.model.email, this.password);
    }
    // close modal
    $('#user-form').modal('toggle');

    // reset form
    form.resetForm();
    this.model = new User();
  }

  onCreate(from: NgForm) {
    console.log('onCreate');

    this.createFlag = true;
    from.resetForm();
    this.model = new User();
  }

  onDelete(user) {
    this.db.list('/users').remove(user.$key);
  }

  createFirebaseUser(email: string, password: string) {
    this.afAuth.auth.createUserWithEmailAndPassword(email, password)
    .then(
      (user) => {
        console.log('firebase user created');
        this.sendEmailVerification(user);
      }
    )
    .catch(
      err => console.log('creating user error ' + err)
    );
  }

  sendEmailVerification(firebaseUser) {
    firebaseUser.sendEmailVerification()
    .then( () => {
      console.log('email sendet');
    })
    .catch(
      () => console.log('erroremail sendet')
    );
  }

  onRightChange(user, event) {
    console.log('onRightChange');
    if (!user.routes || user.routes == null) {
      user.routes = [];
    }

    if (!event.target.checked) {
      user.routes.push(event.target.value);
    } else {
      const idx = user.routes.indexOf(event.target.value);
      if (idx !== -1) {
        user.routes.splice(idx, 1);
      }
    }

    this.db.list('/users').update(user.$key, user);
  }

  ifChecked(user, route): boolean {
    // console.log('if checked');
    if (user.routes && user.routes.length > 0) {
      return user.routes.indexOf(route) === -1;
    }
    return true;
  }
}
